const { spawn } = require("child_process");
const express = require("express");
const app = express();
var path = require("path");
var public = path.join(__dirname, "public");

app.use("/images", express.static(path.join(__dirname, "pubilc")));

const pythonPromise = (data) => {
  return new Promise((resolve, reject) => {
    const python = spawn("python3", ["scatter.py", ...data]);

    python.stdout.on("data", (data) => {
      resolve(data.toString());
    });

    python.stderr.on("data", (data) => {
      reject(data.toString());
    });
  });
};

app.get(
  "/:s1/:s2/:s3/:s4/:s5/:s6/:s7/:s8/:s9/:s10/:water",
  async (req, res) => {
    var s1 = req.params.s1;
    var s2 = req.params.s2;
    var s3 = req.params.s3;
    var s4 = req.params.s4;
    var s5 = req.params.s5;
    var s6 = req.params.s6;
    var s7 = req.params.s7;
    var s8 = req.params.s8;
    var s9 = req.params.s9;
    var s10 = req.params.s10;
    var water = req.params.water;

    var dataFromPython = await pythonPromise([
      s1,
      s2,
      s3,
      s4,
      s5,
      s6,
      s7,
      s8,
      s9,
      s10,
      water,
    ]);

    dataFromPython = dataFromPython.replace(/(\r\n|\n|\r)/gm, "");
    res.setHeader('Access-Control-Allow-Origin', "*");
    res.json({ file: `/images/${dataFromPython}` });
  }
);
app.listen(3200, () => console.log("App is running port 3200"));
